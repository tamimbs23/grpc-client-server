﻿using Greet;
using Grpc.Net.Client;

namespace ClientWeb.Services
{
    public class GrpcDataClient : IGrpcDataClient
    {
        private readonly IConfiguration _configuration;
        public GrpcDataClient(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Task<string> SendGreetings(string firstName, string lastName)
        {
            try
            {
                var address = _configuration["GrpcServer"];
                Console.WriteLine($"SendingGreetings to server : {address}");

                var channel = GrpcChannel.ForAddress(address);
                var client = new GreetingService.GreetingServiceClient(channel);
                var req = new GreetingRequest();
                req.Greeting = new Greeting() { FirstName = firstName, LastName = lastName };
                var res = client.GreetCall(req);
                return Task.FromResult($"Server response: {res}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Can't connect to GRPC Server: {ex.Message}");
                return Task.FromResult($"Error response: {ex.Message}");
            }
        }
    }
}
