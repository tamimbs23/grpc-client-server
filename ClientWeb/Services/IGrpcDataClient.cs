﻿namespace ClientWeb.Services
{
    public interface IGrpcDataClient
    {
        Task<string> SendGreetings(string firstName, string lastName);
    }
}
