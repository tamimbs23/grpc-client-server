﻿using ClientWeb.Models;
using Microsoft.EntityFrameworkCore;

namespace ClientWeb.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Customer> Customers { get; set; }
    }
}
