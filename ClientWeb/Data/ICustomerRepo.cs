﻿using ClientWeb.Models;

namespace ClientWeb.Data
{
    public interface ICustomerRepo
    {
        bool SaveChanges();

        IEnumerable<Customer> GetAllCustomer();
    }
}
