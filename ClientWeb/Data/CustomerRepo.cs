﻿using ClientWeb.Models;

namespace ClientWeb.Data
{
    public class CustomerRepo : ICustomerRepo
    {
        private readonly AppDbContext dbContext;

        public CustomerRepo(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public IEnumerable<Customer> GetAllCustomer()
        {
            return dbContext.Customers.ToList();
        }

        public bool SaveChanges()
        {
            return (dbContext.SaveChanges() >= 0);
        }
    }
}
