﻿using ClientWeb.Data;
using ClientWeb.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ClientWeb.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GrpcController : ControllerBase
    {
        private readonly IGrpcDataClient _grpcDataClient;
        private readonly ICustomerRepo _customerRepo;
        public GrpcController(IGrpcDataClient grpcDataClient, ICustomerRepo customerRepo)
        {
            _grpcDataClient = grpcDataClient;
            _customerRepo = customerRepo;
        }

        [HttpPost]
        public IActionResult SendData(string firstName, string lastName)
        {
            var res = _grpcDataClient.SendGreetings(firstName, lastName);
            return Ok(res);
        }

        [HttpGet]
        public IActionResult GetCustomers()
        {
            var customers = _customerRepo.GetAllCustomer();
            return Ok(customers);
        }
    }
}
