using Microsoft.EntityFrameworkCore;
using ServerWeb.Data;
using ServerWeb.Services.Grpc;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<AppDbContext>(opt =>
            opt.UseSqlServer(builder.Configuration.GetConnectionString("GrpcDb")));

// Add services to the container.

builder.Services.AddGrpc();
builder.Services.AddControllers();
builder.Services.AddScoped<ICustomerRepo, CustomerRepo>();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.MapGrpcService<GrpcDataService>();

app.Run();
