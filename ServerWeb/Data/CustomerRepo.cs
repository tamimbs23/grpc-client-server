﻿using ServerWeb.Models;

namespace ServerWeb.Data
{
    public class CustomerRepo : ICustomerRepo
    {
        private readonly AppDbContext dbContext;

        public CustomerRepo(AppDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public void CreateCustomer(Customer customer)
        {
            if (customer == null)
            {
                throw new ArgumentNullException(nameof(customer));
            }

            dbContext.Customers.Add(customer);
        }

        public bool SaveChanges()
        {
            return (dbContext.SaveChanges() >= 0);
        }
    }
}
