﻿using Microsoft.EntityFrameworkCore;
using ServerWeb.Models;
using System.Collections.Generic;

namespace ServerWeb.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options) { }

        public DbSet<Customer> Customers { get; set; }
    }
}
