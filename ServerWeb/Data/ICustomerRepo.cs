﻿using ServerWeb.Models;

namespace ServerWeb.Data
{
    public interface ICustomerRepo
    {
        void CreateCustomer(Customer customer);
        bool SaveChanges();
    }
}
