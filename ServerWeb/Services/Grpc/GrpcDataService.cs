﻿using Greet;
using Grpc.Core;
using ServerWeb.Data;
using ServerWeb.Models;

namespace ServerWeb.Services.Grpc
{
    public class GrpcDataService : GreetingService.GreetingServiceBase
    {
        private readonly ICustomerRepo _customerRepo;
        public GrpcDataService(ICustomerRepo customerRepo)
        {
            _customerRepo = customerRepo;
        }

        public override Task<GreetingResponse> GreetCall(GreetingRequest req, ServerCallContext context)
        {
            var response = new GreetingResponse();
            response.Result = $"Response from Server: {req.Greeting.FirstName} {req.Greeting.LastName}";

            Customer customer = new Customer();
            customer.FirstName = req.Greeting.FirstName;
            customer.LastName = req.Greeting.LastName;

            try
            {
                _customerRepo.CreateCustomer(customer);
                _customerRepo.SaveChanges();
            }
            catch (Exception ex)
            {
                response.Result += $" Error SaveToDB: {ex.Message}";
            }
            

            return Task.FromResult(response);
        }
    }
}
